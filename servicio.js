var http = require('http');
var fs = require('fs');
 
var server = http.createServer(function (req, res) {
 
    if (req.url === "/asincrona.html") {
        fs.readFile("asincrona.html", 'utf8', function (error, pgResp) {
            if (error) {
                res.writeHead(404);
                res.write('No existe la página');
            } else {
                res.writeHead(200, {'Content-Type': 'text/html' });
                res.write(pgResp);
            }
        res.end();
        });
    } 
    else if (req.url === "/sincrona.html") {
      let archivo = fs.readFileSync('sincrona.html', 'utf-8');
      res.write( archivo );
  } 
  
    
    else {
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write('Hola mundo desde node js!!');
        res.end();
    }
});
server.listen(80);
 
