FROM node:14
LABEL key="Edgar Alejandro Jarquin Flores "
WORKDIR /var/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 80
CMD [ "node", "servicio.js" ]